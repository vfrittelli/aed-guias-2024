def validar_mayor_que(limite, mensaje='Ingrese un numero: '):
    numero = limite
    while numero <= limite:
        numero = int(input(mensaje))
        if numero <= limite:
            print('Error!!!! El numero ingresado debe ser mayor a', limite)
    return numero


def validar_rango(inferior, superior, mensaje='Ingrese un numero: '):
    numero = inferior - 1
    while numero < inferior or numero > superior:
        numero = int(input(mensaje))
        if numero < inferior or numero > superior:
            print('Error!!! El numero debe ser mayor o igual a', inferior, 'y menor igual a', superior)
    return numero


def calcular_promedio(total, cantidad):
    promedio = 0
    if cantidad > 0:
        promedio = total / cantidad
    return promedio
