import random

from clase import *


def validar_positivo(mensaje):
    n = 0
    while n <= 0:
        n = int(input(mensaje))
        if n <= 0:
            print("Valor incorrecto!")
    return n


def cargar_vector(trabajos):
    for i in range(len(trabajos)):
        num = i
        desc= "Descripción " + str(i)
        tipo = random.randint(0, 3)
        imp = random.random() * 10000 + 1
        cant = random.randint(1, 10)
        trabajos[i] = Trabajo(num, desc, tipo, imp, cant)


def mostrar_vector(trabajos):
    for i in range(len(trabajos)):
        print(trabajos[i])


def ordenar(trabajos):
    n = len(trabajos)
    for i in range(n-1):
        for j in range(i+1, n):
            if trabajos[i].importe < trabajos[j].importe:
                trabajos[i], trabajos[j] = trabajos[j], trabajos[i]


def mayor_cantidad(trabajos):
    mayor = trabajos[0]
    for i in range(1, len(trabajos)):
        if trabajos[i].cantidad_personal > mayor.cantidad_personal:
            mayor = trabajos[i]
    return mayor


def buscar(trabajos, d):
    for i in range(len(trabajos)):
        if trabajos[i].descripcion == d:
            return i
    return -1


def contar_por_tipo(trabajos):
    conteo = [0] * 4
    for i in range(len(trabajos)):
        pos = trabajos[i].tipo
        conteo[pos] += 1

    for i in range(len(conteo)):
        if conteo[i] > 0:
            print("Tipo", i, "Cantidad:", conteo[i])


def principal():
    trabajos = []
    carga = False
    op = 1
    while op != 6:
        print("Menú de Opciones")
        print("1-Cargar")
        print("2-Mostrar")
        print("3-Mayor cantidad de personal")
        print("4-Buscar")
        print("5-Contar por tipo")
        print("6-Salir")
        op = int(input("Ingrese su opción: "))

        if op == 1:
            n = validar_positivo("Ingrese la cantidad de trabajos: ")
            trabajos = [None] * n
            cargar_vector(trabajos)
            carga = True
        else:
            if not carga:
                print("Debe cargar datos primero")
            else:
                if op == 2:
                    ordenar(trabajos)
                    mostrar_vector(trabajos)
                elif op == 3:
                    print("Trabajo con mayor cantidad de personal: ")
                    print(mayor_cantidad(trabajos))
                elif op == 4:
                    d = input("Ingrese la descripción a buscar: ")
                    pos = buscar(trabajos, d)
                    if pos == -1:
                        print("No se encontró!")
                    else:
                        print("Encontrado:")
                        print(trabajos[pos])
                elif op == 5:
                    contar_por_tipo(trabajos)
        if op == 6:
            print("Bye!")


if __name__ == "__main__":
    principal()

