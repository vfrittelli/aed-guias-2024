class Trabajo:
    def __init__(self, numero, descripcion, tipo, importe, cantidad):
        self.numero = numero
        self.descripcion = descripcion
        self.tipo = tipo
        self.importe = importe
        self.cantidad = cantidad

    def __str__(self):
        res = "Número: " + str(self.numero) + ", Descripción: "+ str(self.descripcion) + \
              ", Tipo: " + str(self.tipo) + ", Importe: " + str(self.importe) + ", Cantidad: " + \
            str(self.cantidad)
        return res
