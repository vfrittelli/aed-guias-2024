class Alumno:
    def __init__(self, dni, nombre, apellido, dni_tutor, importe, nivel):
        self.dni = dni
        self.nombre = nombre
        self.apellido = apellido
        self.dni_tutor = dni_tutor
        self.importe = importe
        self.nivel = nivel

    def __str__(self):
        return f'| {self.dni:<10} | {self.nombre:<10} | {self.apellido:<10} | ' \
                f'{self.dni_tutor:<10} | ${self.importe:>10.2f} | {self.nivel:^5} |'
