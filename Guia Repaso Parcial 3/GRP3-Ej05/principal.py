import random

from clase import Alumno
from generales import *


def menu():
    cadena = 'Menu de Opciones\n' \
             '=================================================\n' \
             '1 ---- Cargar Arreglo de Alumnos\n' \
             '2 ---- Mostrar los datos de los Alumnos\n' \
             '3 ---- Determinar Cantidad de Alumnos por Nivel\n' \
             '4 ---- Determinar Monto Total a Abonar por Tutor\n' \
             '5 ---- Buscar Alumno\n' \
             '0 ---- Salir\n' \
             'Ingrese su opcion: '
    return int(input(cadena))


def cargar_alumnos(vector, n):
    nombres = 'Carlos', 'Juan', 'Karina', 'Laura', 'Joaquin', 'Ignacio', 'Irina'
    apellidos = 'Guaranies', 'Aztecas', 'Andaluces', 'Romanos', 'Griegos', 'Fenicios', 'Persas'

    for i in range(n):
        dni = f'{random.randint(35, 99)}.{random.randint(100, 999)}.{random.randint(100, 999)}'
        nombre = random.choice(nombres)
        apellido = random.choice(apellidos)
        dni_tutor = f'{random.randint(10, 30)}.{random.randint(100, 999)}.{random.randint(100, 999)}'
        importe = random.uniform(2500, 7500)
        nivel = random.randrange(12)
        alumno = Alumno(dni, nombre, apellido, dni_tutor, importe, nivel)
        vector.append(alumno)


def ordenar(vector):
    tam = len(vector)
    for i in range(tam - 1):
        for j in range(i + 1, tam):
            if vector[i].apellido > vector[j].apellido:
                vector[i], vector[j] = vector[j], vector[i]


def mostrar(vector):
    print(f'| {"DNI":^10} | {"Nombre":^10} | {"Apellido":^10} | '
          f'{"Dni Tutor":^10} | ${"Importe":^10} | {"Nivel":^5} |')
    for alumno in vector:
        print(alumno)


def acumular_por_nivel(vector):
    v = [0] * 13
    for alumno in vector:
        v[alumno.nivel] += 1
    return v


def total_pagar_tutor(vector, x):
    total = 0
    for alumno in vector:
        if alumno.dni_tutor == x:
            total += alumno.importe
    return total


def buscar(vector, x):
    for alumno in vector:
        if alumno.dni == x:
            return alumno
    return None


def principal():
    opcion = -1
    alumnos = []

    while opcion != 0:
        opcion = menu()
        if opcion == 1:
            n = cargar_mayor_que(0, 'Ingrese la cantidad de Alumnos a cargar: ')
            cargar_alumnos(alumnos, n)

        if len(alumnos) > 0:
            if opcion == 2:
                ordenar(alumnos)
                mostrar(alumnos)

            elif opcion == 3:
                vc = acumular_por_nivel(alumnos)
                for i in range(len(vc)):
                    if vc[i] > 0:
                        print(f'Nivel {i} tiene un total de {vc[i]} alumnos')

            elif opcion == 4:
                dni_tutor = input('Ingrese el dni del tutor: ')
                total = total_pagar_tutor(alumnos, dni_tutor)
                print(f'El total a abonar por el tutor es de ${total:<10.2f}')

            elif opcion == 5:
                dni = input('Ingrese el dni del alumno a buscar: ')
                encontrado = buscar(alumnos, dni)
                if encontrado:
                    encontrado.importe *= 1.10
                    print(f'| {"DNI":^10} | {"Nombre":^10} | {"Apellido":^10} | '
                          f'{"Dni Tutor":^10} | ${"Importe":^10} | {"Nivel":^5} |')
                    print(encontrado)
                else:
                    print('No existe un alumno con el dni ingresado')
        else:
            print('Primero debe generar el arreglo de Alumnos')


if __name__ == '__main__':
    principal()
