# AED Guías 2024

Repositorio con las guías de ejercicios y sus soluciones para el ciclo lectivo 2024


## Listado de Guías

- [Guía 01 - Fundamentos de Programación](./Guia%2001)
- [Guía 02 - Estructuras Secuenciales](./Guia%2002)
- [Guía 03 - Tipos Estructurados Básicos](./Guia%2003)
- [Guía 04 - Estructuras Condicionales](./Guia%2004)
- [Guía 05 - Estructuras Condicionales - Variantes](./Guia%2005)
- [Guía 06 - Estructuras Repetitivas - Ciclo while](./Guia%2006)
- [Guía 07 - Estructuras Repetitivas - Ciclo for](./Guia%2007)
- [Guía 08 - Estructuras Repetitivas - Variantes](./Guia%2008)
- [Guía 09 - Procesamiento de Secuencias de Caracteres](./Guia%2009)
- [Guía 10 - Subproblemas y Funciones](./Guia%2010)
- [Guía 11 - Funciones - Implementación Detallada](./Guia%2011)
- [Guía 12 - Programación Modular](./Guia%2012)
- [Guía 13 - Módulos y Paquetes](./Guia%2013)
- [Guía 14 - Recursividad](./Guia%2014)
- [Guia 15 - Arreglos Unidimensionales](./Guia%2015)
- [Guia 16 - Ordenamiento y Búsqueda](./Guia%2016)
- [Guia 17 - Arreglos - Casos de Estudio I](./Guia%2017)
- [Guia 18 - Arreglos - Casos de Estudio II](./Guia%2018)
- [Guia 19 - Registros y Objetos](./Guia%2019)
- [Guia 20 - Arreglos de Objetos](./Guia%2020)
- [Guia 21 - Arreglos Bidimensionales](./Guia%2021)
- [Guia Repaso Parcial 2](./Guia%20Repaso%20Parcial%202)
- [Guia Repaso Parcial 3](./Guia%20Repaso%20Parcial%203)

