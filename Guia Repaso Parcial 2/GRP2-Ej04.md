### Repaso Parcial 2 - Ejercicio 4
Se pide desarrollar un programa en Python que permita cargar por teclado un texto completo en una variable de tipo cadena de caracteres. El texto finaliza con ‘.’ y se supone que el usuario cargará el punto para indicar el final del texto, y que cada palabra de ese texto está separada de las demás por un espacio en blanco. El programa debe:

1. Determinar la cantidad de palabras que incluyen exactamente dos vocales. Por ejemplo, en el texto: “Los cursos del turno tarde.” Respuesta: 3 palabras. Las palabras que cumplen con la condición son: “cursos”, “turno” y “tarde”. 

2. Determinar el porcentaje de palabras en todo el texto que tienen una cantidad impar de caracteres. Por ejemplo, en el texto: “Hoy es un lindo día.” Respuesta: 60,00 % (palabras que cumplen la condición: 3 ("Hoy", "lindo" y "día") sobre un total de palabras de 5).

3. Determinar cuántas palabras del texto inician con consonante y tienen más de 4 letras. Por ejemplo, en el texto: “Confiamos que aprueben el parcial.” Respuesta: 2 palabras. Las palabras que cumplen la condición son: “Confiamos” y “parcial”.

4. Determinar cuántas palabras incluyen la expresión "sa" pero más de una vez. Por ejemplo, en el texto: “La salsa es muy sabrosa.” Respuesta: 2 palabras. Las palabras que cumple con la condición son: “salsa” y "sabrosa".
