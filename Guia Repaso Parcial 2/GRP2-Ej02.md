### Repaso Parcial 2 - Ejercicio 2
Se pide desarrollar un programa en Python que permita cargar por teclado un texto completo en una variable de tipo cadena de caracteres. El texto finaliza con ‘.’ y se supone que el usuario cargará el punto para indicar el final del texto, y que cada palabra de ese texto está separada de las demás por un espacio en blanco. El programa debe:

1. Determinar la cantidad de palabras que presentan digitos en la palabra. Por ejemplo, en el texto: “El 1K09 y el 1K15 son cursos espejos a partir del año 2020.” Tiene 3 palabras que cumplen con la condición: “1K09”, “1K15” y “2020”.

2. Determinar el promedio de letras de la palabras del texto que presentan la combinacion “pe” a partir de la tercer letra de la palabra. Por ejemplo, en el texto: “El acampe del año apenas tuvo un ágape.” Tiene 2 palabras que cumplen con la condición “acampe” y “ágape” que suman 11 letras, por lo que el promedio seria 11/2 = 5.5 letras.

3. Determinar la cantidad de palabras que finalizan con la letra “n” y que empiecen con una vocal. Por ejemplo, en el texto: “Los jovenes acamparon y asaron unos choripanes.” Tiene 2 palabras que cumplen con la condición “acamparon” y “asaron”.

4. Determinar la cantidad de palabras que finalizan con la primera letra de esa palabra. Por ejemplo, en el texto: “Las simples vidas son solitarias.” Tiene 2 palabras que cumplen con la condición “simples” y “solitarias”.
