### Repaso Parcial 2 - Ejercicio 3
Se pide desarrollar un programa en Python que permita cargar por teclado un texto completo en una variable de tipo cadena de caracteres. El texto finaliza con ‘.’ y se supone que el usuario cargará el punto para indicar el final del texto, y que cada palabra de ese texto está separada de las demás por un espacio en blanco. El programa debe:

1. Determinar la cantidad de palabras que contiene la letra "l" en la primera mitad de la palabra y terminan con "s". Por ejemplo, en el texto: "Los elementos allanados seran totalizados en el juzgado." Tiene 4 palabras que cumplen con la condición: "Los", "elementos" "totalizados" y "allanados".

2. Determinar el porcentaje que representan las palabras de longitud impar con respecto del total de palabras del texto. Por ejemplo, en el texto: "La carta se entrega en el correo por la mañana." hay 3 palabras de longitud impar, y hay 10 palabras en el texto, por lo que el porcentaje seria de 3 * 100 / 10 = 30%. 

3. Determinar la cantidad de palabras que contiene la secuencia "pl" a partir de la tercer letra. Por ejemplo, en el texto: "El empleado tenia que emplear un pegamento para unir el plegado." Tiene 2 palabras que cumplen con la condición: "empleado" y "emplear".

4. Determinar la cantidad de palabras que tienen mayor cantidad de dígitos que letras, y tengan además al menos una letra. Por ejemplo, en el texto: "Los cursos 1K09 y 1K15 son espejos desde el 2020." tiene 2 palabras que cumplen con la condición: "1K09" y "1K15".
