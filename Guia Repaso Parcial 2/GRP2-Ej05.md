### Repaso Parcial 2 - Ejercicio 5
Se pide desarrollar un programa en Python que permita cargar por teclado un texto completo en una variable de tipo cadena de caracteres. El texto finaliza con ‘.’ y se supone que el usuario cargará el punto para indicar el final del texto, y que cada palabra de ese texto está separada de las demás por un espacio en blanco. El programa debe:

1. Determinar la cantidad de palabras que incluyen al menos una vocal. Por ejemplo, en el texto: “Los cursos 1k4 y 1k8.” Respuesta: 2 palabras. La palabras que cumplen con la condición son: “Los” y “cursos”.

2. Determinar cuántas palabras incluyen la expresión ‘sa’ en la primera mitad de la palabra. Por ejemplo, en el texto: “una saludable siesta y luego comer salsa bolognesa.” Respuesta: 2 palabras. Las palabras que cumplen con la condición son: “saludable” y “salsa”.

3. Determinar el porcentaje de palabras en todo el texto que tienen una cantidad par de caracteres. Por ejemplo, en el texto: “La zapatilla de temporada.” Respuesta: 50,00 %. Palabras que cumplen la condición: 2, total de palabras: 4. Porcentaje = 2 * 100 / 4 = 50.00%.

4. Determinar cuántas palabras incluyen el último caracter de la primera palabra ingresada en el texto. Por ejemplo, en el texto: “esperamos les guste la materia.” Respuesta: 2 palabras. Las palabras que cumplen la condición son: “les” y “guste”. El último caracter de la primera palabra es ‘s’, y las dos palabra citadas tienen alguna 's' en alguna parte.
