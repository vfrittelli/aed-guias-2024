### Repaso Parcial 2 - Ejercicio 1
Se pide desarrollar un programa en Python que permita cargar por teclado un texto completo en una variable de tipo cadena de caracteres. El texto finaliza con ‘.’ y se supone que el usuario cargará el punto para indicar el final del texto, y que cada palabra de ese texto está separada de las demás por un espacio en blanco. El programa debe:

1. Determinar la cantidad de palabras que terminan con la tercer letra de la primera palabra del texto. Por ejemplo, en el texto: “Las casas se terminaron antes de lo previsto.” Tiene 2 palabras que cumplen con la condición: “casas” y “antes”.

2. Determinar la cantidad palabras que tienen vocales en la segunda mitad de la palabra y cuya longitud sea mayor o igual a 5 letras. Por ejemplo, en el texto “Las materias de la facultad en cuarentena se rinden en aula virtual.” Tiene 5 palabras que cumplen con la condición: “materias”, “facultad”, “rinden”, ”cuarentena” y “virtual”.

3. Determinar la cantidad de palabras de longitud impar que se encuentran en el texto y que comienzan con una consonante. Por ejemplo, en el texto: “En el aula virtual los alumnos realizan tareas.” Tiene 2 palabras que cumplen con la condición: “virtual” y “realizan”.

4. Determinar el porcentaje que representan las palabras del punto 3 en base al total de palabras que se encuentran en el texto. Por ejemplo, en el texto: “En el aula virtual los alumnos realizan tareas.” La cantidad de palabras son 8, 2 palabras cumplen con el punto 3, el porcentaje seria 2/8 = 25%.
