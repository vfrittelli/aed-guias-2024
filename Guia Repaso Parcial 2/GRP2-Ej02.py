def es_digito(caracter):
    return caracter in "0123456789"


def calcular_promedio(cantidad, total):
    promedio = 0
    if total > 0:
        promedio = cantidad / total
    return promedio


def es_vocal(caracter):
    return caracter in "aeiouAEIOUáéíóúÁÉÍÓÚ"


def principal():
    print("Primer Ejericio Repaso Parcial 2")
    print("=" * 60)

    texto = input("Ingrese el texto a procesar (debe finalizar con punto): ")
    texto = texto.lower()

    cont_letras = 0
    tiene_digitos = False
    cont_palabras = cont_palabras_con_digito = cont_palabras_vocal_fin_n = cont_palabras_fin_misma_letra = 0
    total_letras = 0
    tiene_pe = tiene_p = empieza_vocal = False
    pri_letra = ""

    for caracter in texto:
        if caracter != " " and caracter != ".":
            cont_letras += 1

            if es_digito(caracter):
                tiene_digitos = True

            if cont_letras > 3:
                if caracter == "p":
                    tiene_p = True
                else:
                    if caracter == "e" and tiene_p:
                        tiene_pe = True
                    tiene_p = False

            if cont_letras == 1:
                pri_letra = caracter
                if es_vocal(caracter):
                    empieza_vocal = True

        else:

            cont_palabras += 1

            if tiene_digitos:
                cont_palabras_con_digito += 1

            if tiene_pe:
                total_letras += cont_letras

            if empieza_vocal and anterior == "n":
                cont_palabras_vocal_fin_n += 1

            if pri_letra == anterior:
                cont_palabras_fin_misma_letra += 1

            tiene_digitos = False
            empieza_vocal = False
            tiene_pe = False
            cont_letras = 0
        anterior = caracter

    print("La cantidad de palabras que contienen digitos son:", cont_palabras_con_digito)
    promedio = calcular_promedio(total_letras, cont_palabras)
    print("El promedio de letras por palabra que tienen \"pe\" a partir de la tercer letra es:", round(promedio, 2))
    print("La cantidad de palabras que comienzan con vocal y terminan con \"n\" son:", cont_palabras_vocal_fin_n)
    print("La cantidad de palabras que comienzan y terminan con la misma letra son:", cont_palabras_fin_misma_letra)


if __name__ == '__main__':
    principal()