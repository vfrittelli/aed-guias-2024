def es_vocal(caracter):
    return caracter in "aeiouáéíóú"


def calcular_porcentaje(cantidad, total):
    porcentaje = 0
    if total > 0:
        porcentaje = cantidad * 100 / total
    return porcentaje


def es_consonante(caracter):
    digitos = "0123456789"
    return not es_vocal(caracter) and caracter not in digitos


def principal():
    print("Ejericio Repaso Segundo Parcial")
    print("=" * 60)

    texto = input("Ingrese el texto que desee procesar (debe finalizar con un punto): ")
    texto = texto.lower()

    cont_letras = cont_vocales = cont_sa = 0
    cont_palabras = cont_palabras_2_vocales = cont_palabras_long_impar = cont_palabras_conso_4_letras = 0
    cont_palabras_sa_veces = 0
    comienza_conso = tiene_s = False

    for caracter in texto:
        if caracter != " " and caracter != ".":
            cont_letras +=1

            if es_vocal(caracter):
                cont_vocales += 1

            if cont_letras == 1 and es_consonante(caracter):
                comienza_conso = True

            if caracter == "s":
                tiene_s = True
            else:
                if tiene_s and caracter == "a":
                    cont_sa += 1
                tiene_s = False

        else:
            cont_palabras += 1

            if cont_vocales == 2:
                cont_palabras_2_vocales += 1

            if cont_letras % 2 == 1:
                cont_palabras_long_impar += 1

            if cont_letras > 4 and comienza_conso:
                cont_palabras_conso_4_letras += 1

            if cont_sa > 1:
                cont_palabras_sa_veces += 1

            cont_letras = cont_vocales = 0
            comienza_conso = False

    print("La cantidad de palabras que tienen solo dos vocales son:", cont_palabras_2_vocales)

    porcentaje = calcular_porcentaje(cont_palabras_long_impar, cont_palabras)
    print("La cantidad de palabras que tienen una longitud impar de caracteres son:", cont_palabras_long_impar,
          "y representan un", round(porcentaje, 2), "% sobre el total de palabras del texto")

    print("La cantidad de palabras que comienzan con consonante y tiene mas de 4 letras son:",
          cont_palabras_conso_4_letras)

    print("La cantidad de palabras que contienen la silaba \"sa\" mas de una vez son:", cont_palabras_sa_veces)


if __name__ == '__main__':
    principal()