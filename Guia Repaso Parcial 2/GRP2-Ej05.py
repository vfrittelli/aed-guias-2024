def calcular_porcentaje(cantidad, total):
    porcentaje = 0
    if total > 0:
        porcentaje = cantidad * 100 / total
    return porcentaje


def es_vocal(caracter):
    return caracter in "aeiouAEIOUáéíóúÁÉÍÓÚ"


def principal():
    print("Primer Ejericio Repaso Parcial 2")
    print("=" * 60)

    texto = input("Ingrese el texto a procesar (debe finalizar con punto): ")
    texto = texto.lower()

    cont_letras = cont_vocales = cont_palabras = 0
    cont_palabras_con_vocal = cont_palabras_sa_pri_mitad = cont_palabras_longitud_par = cont_palabras_con_ult_car = 0
    tiene_s = tiene_sa = tiene_ult_car_pri_palabra = False
    pos_pri_sa = 0
    ultimo_pri_palabra = " "

    for caracter in texto:
        if caracter != " " and caracter != ".":
            cont_letras += 1

            if es_vocal(caracter):
                cont_vocales += 1

            if caracter == "s":
                tiene_s = True
            else:
                if tiene_s and caracter == "a":
                    tiene_sa = True
                    if pos_pri_sa == 0:
                        pos_pri_sa = cont_letras
                tiene_s = False

            if cont_palabras == 0:
                ultimo_pri_palabra = caracter
            else:
                if caracter == ultimo_pri_palabra:
                    tiene_ult_car_pri_palabra = True

        else:
            if cont_letras > 0:
                cont_palabras += 1

                if cont_vocales >= 1:
                    cont_palabras_con_vocal += 1

                if tiene_sa and pos_pri_sa <= cont_letras // 2:
                    cont_palabras_sa_pri_mitad += 1

                if cont_letras % 2 == 0:
                    cont_palabras_longitud_par += 1

                if cont_palabras > 1 and tiene_ult_car_pri_palabra:
                    cont_palabras_con_ult_car += 1

            cont_letras = cont_vocales = 0
            pos_pri_sa = 0
            tiene_sa = tiene_ult_car_pri_palabra = False

    print("La cantidad de palabras que contienen al menos una vocal son:", cont_palabras_con_vocal)

    print("La cantidad de palabras que contiene \"sa\" en la primera mitad de la palabra son:",
          cont_palabras_sa_pri_mitad)

    porcentaje = calcular_porcentaje(cont_palabras_longitud_par, cont_palabras)
    print("La cantidad de palabras que tienen longitud par son:", cont_palabras_longitud_par, "y representan el",
          round(porcentaje, 2), "% sobre el total de palabras")

    print("La cantidad de palabras que incluyen el último caracter de la primera palabra ingresada en el texto son:",
          cont_palabras_con_ult_car)


if __name__ == '__main__':
    principal()